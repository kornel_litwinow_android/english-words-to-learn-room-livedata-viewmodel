package kornel.litwinow.learnenglishwords;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class WordViewModel extends AndroidViewModel {

    private WordRepository wordRepository;
    private LiveData<List<Word>> allWords;


    public WordViewModel( Application application) {
        super(application);

        wordRepository = new WordRepository(application);
        allWords = wordRepository.getAllWords();
    }

    public void insert(Word word) {
        wordRepository.insert(word);
    }

    public void update(Word word) {
        wordRepository.update(word);
    }

    public void delete(Word word) { wordRepository.delete(word);
    }

    public LiveData<List<Word>> getAllWords() {
        return allWords;
    }

}
