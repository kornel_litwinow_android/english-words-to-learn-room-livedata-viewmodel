package kornel.litwinow.learnenglishwords;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyler_view)
    RecyclerView recyclerView;
    @BindView(R.id.button_add)
    FloatingActionButton buttonAdd;

    public static final int ADD_WORD_REQUEST = 1;
    public static final int EDIT_WORD_REQUEST = 2;
    private WordViewModel viewModel;
    WordAdapter wordAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        openAddEditWordActivity();
        initRecyclerView();
        provideViewModel();
        touchHelper();
    }

    public void openAddEditWordActivity() {
        buttonAdd.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, AddEditWordActivity.class);
            startActivityForResult(intent, ADD_WORD_REQUEST);
        });
    }

    public void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        wordAdapter = new WordAdapter();
        recyclerView.setAdapter(wordAdapter);

        wordAdapter.setOnItemClickListener(word -> {
            Intent intent = new Intent(MainActivity.this, AddEditWordActivity.class);
            intent.putExtra(AddEditWordActivity.EXTRA_ID, word.getId());
            intent.putExtra(AddEditWordActivity.EXTRA_WORD_ENGLISH, word.getWordEnglish());
            intent.putExtra(AddEditWordActivity.EXTRA_WORD_POLISH, word.getWordPolish());

            startActivityForResult(intent, EDIT_WORD_REQUEST);
        });
    }

    public void provideViewModel() {
        viewModel = ViewModelProviders.of(this).get(WordViewModel.class);
        viewModel.getAllWords().observe(this, words -> wordAdapter.setWords(words));
    }

    public  void touchHelper(){
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                viewModel.delete(wordAdapter.getWordAt(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "Note deleted", Toast.LENGTH_SHORT).show();
            }

        }).attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_WORD_REQUEST && resultCode == RESULT_OK) {
            assert data != null;
            String wordEnglish = data.getStringExtra(AddEditWordActivity.EXTRA_WORD_ENGLISH);
            String wordPolish = data.getStringExtra(AddEditWordActivity.EXTRA_WORD_POLISH);
            Word word = new Word(wordPolish,wordEnglish);
            viewModel.insert(word);

            Toast.makeText(this, "Word Saved", Toast.LENGTH_SHORT).show();
        } else if (requestCode == EDIT_WORD_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddEditWordActivity.EXTRA_ID, -1);

            if (id == -1) {
                Toast.makeText(this, "Word cant be updated", Toast.LENGTH_SHORT).show();
                return;
            }
            String wordEnglish = data.getStringExtra(AddEditWordActivity.EXTRA_WORD_ENGLISH);
            String wordPolish = data.getStringExtra(AddEditWordActivity.EXTRA_WORD_POLISH);

            Word word = new Word(wordPolish,wordEnglish);
            word.setId(id);
            viewModel.update(word);
            Toast.makeText(this, "Word updated", Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "Word not saved", Toast.LENGTH_SHORT).show();
        }
    }
}
