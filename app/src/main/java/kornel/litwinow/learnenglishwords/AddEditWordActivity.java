package kornel.litwinow.learnenglishwords;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddEditWordActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_word_english)
    EditText editTextWordEnglish;
    @BindView(R.id.edit_text_word_polish)
    EditText editTextWordPolish;
    @BindView(R.id.button_save)
    Button buttonSave;


    public static final String EXTRA_ID =
            "kornel.litwinow.learnenglishwords.EXTRA_ID";

    public static final String EXTRA_WORD_ENGLISH =
            "kornel.litwinow.learnenglishwords.EXTRA_WORD_ENGLISH";

    public static final String EXTRA_WORD_POLISH =
            "kornel.litwinow.learnenglishwords.EXTRA_WORD_POLISH";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_word);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        if (intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit word");
            editTextWordEnglish.setText(intent.getStringExtra(EXTRA_WORD_ENGLISH));
            editTextWordPolish.setText(intent.getStringExtra(EXTRA_WORD_POLISH));
        } else {
            setTitle("Add word");
        }

        buttonSave.setOnClickListener(v -> {
            saveWord();
        });
    }

    private void saveWord() {
        String wordEnglish = editTextWordEnglish.getText().toString();
        String wordPolish = editTextWordPolish.getText().toString();


        if (wordEnglish.trim().isEmpty() || wordPolish.trim().isEmpty()) {
            Toast.makeText(this, "Please insert words", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_WORD_POLISH, wordPolish);
        data.putExtra(EXTRA_WORD_ENGLISH, wordEnglish);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);

        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }
}
