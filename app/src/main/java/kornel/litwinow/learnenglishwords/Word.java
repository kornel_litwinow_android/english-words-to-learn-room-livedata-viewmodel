package kornel.litwinow.learnenglishwords;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "word_table")
public class Word {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String wordPolish;
    private String wordEnglish;

    public Word(String wordPolish, String wordEnglish) {
        this.wordPolish = wordPolish;
        this.wordEnglish = wordEnglish;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getWordPolish() {
        return wordPolish;
    }

    public String getWordEnglish() {
        return wordEnglish;
    }
}
