package kornel.litwinow.learnenglishwords;

import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class WordAdapter extends RecyclerView.Adapter<WordAdapter.WordHolder> {

    private List<Word> words = new ArrayList<>();
    private OnItemClickListener listener;


    @NonNull
    @Override
    public WordHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.word_item, parent, false);
        return new WordHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WordHolder holder, int position) {
        Word currentWord = words.get(position);
        holder.textViewWordPolish.setText(currentWord.getWordPolish());
        holder.textViewWordEnglish.setText(currentWord.getWordEnglish());

    }

    @Override
    public int getItemCount() {
        return words.size();
    }

    public void setWords(List<Word> words) {
        this.words = words;
        notifyDataSetChanged();
    }

    public Word getWordAt(int position) {
        return words.get(position);
    }


    class WordHolder extends RecyclerView.ViewHolder {
        private TextView textViewWordEnglish;
        private TextView textViewWordPolish;


        public WordHolder(View itemView) {
            super(itemView);
            textViewWordEnglish = itemView.findViewById(R.id.text_view_word_english);
            textViewWordPolish = itemView.findViewById(R.id.text_view_word_polish);

            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(words.get(position));
                }
            });


        }
    }

    public interface OnItemClickListener {
        void onItemClick(Word word);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
